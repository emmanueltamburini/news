FROM openjdk:11-jre

ENTRYPOINT ["java", "-jar", "/opt/news/news-app.jar", "--spring.config.location=classpath:/application.properties,file:/opt/news/config/application.properties"]

ARG JAR_FILE
ADD target/${JAR_FILE} /opt/news/news-app.jar

RUN mkdir -p /opt/news/logs/