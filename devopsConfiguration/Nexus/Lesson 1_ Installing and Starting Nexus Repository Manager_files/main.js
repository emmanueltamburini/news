document.addEventListener('DOMContentLoaded', function() {

    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function($el) {
            $el.addEventListener('click', function() {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});

//Sidebar Tree Menu
(function($) {

    'use strict';

    // http://learn.jquery.com/plugins/basic-plugin-creation/
    // http://learn.jquery.com/plugins/advanced-plugin-concepts/
    $.fn.scrollTree = function(options) {

        var DEFAULT_OPTIONS = {
            'contextPath': '/',
            'css': {
                'ancestor': 'active',
                'current': 'active',
                "leaf": 'leaf',
                'loading': 'sp-loading',
                'collapsed': 'mdi-chevron-right',
                'expanded': 'mdi-chevron-down',
                'error': 'sp-error'
            },
            'renderChildrenUl': function() {
                return '<ul class="nav"></ul>';
            },
            'renderChildLi': function (child, opts) {
                if (child.title == "sitemap.xml") {
                    return '';
                }
                return '<li class="' + opts.css[child.type] + '"><span class="sp-toggle mdi"></span><a href="' + child.link + '">' + child.title + '</a></li>'
            }
        };

        var viewportId = $(this).data('viewportId');
        var rootLink = $(this).data('root');
        var currentLink = $(this).data('current');

        var opts = $.extend(true, DEFAULT_OPTIONS, options);

        return this.each(function() {
            var $rootUl = $(this);

            loadChildren($rootUl, rootLink, currentLink);
            setupEventHandling($rootUl);

            return this;
        });


        function loadChildren($ul, parentLink, currentLink) {
            var $parentLi = $ul.closest('li');
            var $span = $parentLi.children('span');
            if ($parentLi) {
                $span.removeClass(opts.css.collapsed)
                    .addClass(opts.css.loading);
            }

            $.get(opts.contextPath + '/rest/scroll-viewport/1.0/tree/children', {
                    'viewportId': viewportId,
                    'root': rootLink,
                    'parent': parentLink || $parentLi.find('> a').attr('href'),
                    'current': currentLink || ''
                })
                .success(function success(children) {
                    insertChildren($ul, children);

                    $span.removeClass(opts.css.loading)
                        .addClass(opts.css.expanded);
                })
                .error(function error(jqXHR, textStatus, errorThrown) {
                    $span.removeClass(opts.css.loading)
                        .addClass(opts.css.error);
                });
        }


        function insertChildren($ul, children) {
            $ul.html('');
            $.each(children, function(idx, child) {
                var $childLi = $(opts.renderChildLi(child, opts)).appendTo($ul);

                if (child.children) {
                    if (child.children.length) {
                        $childLi.children('span').addClass(opts.css.expanded);
                        var $childrenEl = $(opts.renderChildrenUl()).appendTo($childLi);
                        insertChildren($childrenEl, child.children);

                    } else {
                        $childLi.children('span').addClass(opts.css.collapsed);
                    }
                } else {
                    $childLi.addClass(opts.css.leaf);
                }
            });
        }


        function setupEventHandling($rootUl) {
            $rootUl.on('click', '.sp-toggle', function() {
                var $li = $(this).parent('li');
                var $span = $li.children('span');
                console.log($span);
                if ($span.is('.' + opts.css.collapsed)) {
                    openNode($li);

                } else if ($span.is('.' + opts.css.expanded)) {
                    closeNode($li);

                } else {

                    // we don't have children -> no-op
                }
            });
        }


        function openNode($li) {
            var $span = $li.children('span');
            if ($li.has('ul').length) {
                // children have been loaded, just toggle classes
                var $ul = $li.children('ul');
                $ul.show();
                $span.removeClass(opts.css.collapsed)
                    .addClass(opts.css.expanded);
            } else {
                // children have to be loaded
                var $childrenEl = $(opts.renderChildrenUl()).appendTo($li);
                loadChildren($childrenEl);
            }
        }


        function closeNode($li) {
            var $span = $li.children('span');
            var $ul = $li.children('ul');
            $ul.hide();
            $span.removeClass(opts.css.expanded)
                .addClass(opts.css.collapsed);
        }
    };

})(jQuery);

// Mobile Menu