const IS_SIMPLE = false;  // layout view
//# of results returned for each index
const DOCS_HIT_LIMIT = 5;
const KB_HIT_LIMIT = 2;
const COMMUNITY_HIT_LIMIT = 2;
const GUIDES_HIT_LIMIT = 2
const ZENDESK_BASEURL = 'https://support.sonatype.com/hc/';  // prod site
const COMMUNITY_BASEURL = 'https://community.sonatype.com';
const GUIDES_BASEURL = 'https://guides.sonatype.com';
const ZENDESK_INDEX = 'zendesk_sonatype_articles';  // prod site
const DOCS_INDEX = 'help_site_docs';
const COMMUNITY_INDEX = 'discourse-posts';
const GUIDES_INDEX = 'guides_site';

document.addEventListener('DOMContentLoaded', () => {
  var client = algoliasearch('92C8QC4EC7', 'dabe3bd422efc94d2d073a22ef8baa91');

  var autocompleteOptions = {
    openOnFocus: true,
    autoWidth: false,
    minLength: 2,
    keyboardShortcuts: ['s', '/'],
    cssClasses: {prefix: 'ds'},
    templates: {
      empty: docsearch.getEmptyTemplate(),
      header: '<p class="algolia-header">[enter] for full results</p>'
    }
  }

  var search = autocomplete(
      '#aa-search-input', autocompleteOptions, [{
        name: 'search',
        source: function(query, callback) {
          const queries = [
            getQuery(DOCS_INDEX, DOCS_HIT_LIMIT, query, 'content:25'),
            getQuery(COMMUNITY_INDEX, COMMUNITY_HIT_LIMIT, query),
            getQuery(ZENDESK_INDEX, KB_HIT_LIMIT, query, 'body_safe:25'),
            getQuery(GUIDES_INDEX, GUIDES_HIT_LIMIT, query)
          ];

          client.search(queries, (err, {results} = {}) => {
            if (err) callback([]);  // throw err;

            var formattedHits = formatResults(results);
            callback(formattedHits);
          });
        },
        templates: {suggestion: docsearch.getSuggestionTemplate(IS_SIMPLE)}
      }]);

  search.on('autocomplete:selected', function (event, suggestion, datasetName) {
    keyboardSelected = true;
    window.location.href = suggestion.url;
  });

  function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'), sParameterName, i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };

  var query = getUrlParameter('q');
  if (query) {
    search.autocomplete.setVal(query);
    search.autocomplete.open();
  }
});

function formatResults(results) {
  var formattedHits = [];
  for (var i = 0; i < results.length; i++) {
    var index = results[i];
    var hits;
    switch (index.index) {
      case DOCS_INDEX:
        // Help
        hits = formatDocHits(index.hits);
        break;
      case COMMUNITY_INDEX:
        // Community
        hits = formatCommunityHits(index.hits, COMMUNITY_BASEURL);
        break;
      case ZENDESK_INDEX:
        // Articles
        var hits = formatZenDeskHits(index.hits, ZENDESK_BASEURL);
        break;
      case GUIDES_INDEX:
        // Guides
        hits = formatGuidesHits(index.hits, GUIDES_BASEURL);
        break;
    }
    // formattedHits.push(...hits);
    Array.prototype.push.apply(formattedHits, hits);
  }
  return formattedHits;
}

function formatDocHits(hits) {
  return hits.map(function(hit, i) {
    const url = hit.url
    const category = 'Sonatype Documentation';
    const subcategory = (hit._highlightResult.space.value || hit.space);
    const displayTitle = (hit._highlightResult.name.value || hit.name);
    const text = hit._snippetResult.content.value;

    const isTextOrSubcategoryNonEmpty = (subcategory && subcategory !== '') ||
        (displayTitle && displayTitle !== '');
    const isLvl1EmptyOrDuplicate =
        !subcategory || subcategory === '' || subcategory === category;
    const isLvl2 =
        displayTitle && displayTitle !== '' && displayTitle !== subcategory;
    const isLvl1 = !isLvl2 &&
        (subcategory && subcategory !== '' && subcategory !== category);
    const isLvl0 = !isLvl1 && !isLvl2;
    const isHeader = (i === 0);
    return {
      isLvl0,
      isLvl1,
      isLvl2,
      isLvl1EmptyOrDuplicate,
      isCategoryHeader: isHeader,  // hit.isCategoryHeader,
      isSubCategoryHeader: hit.isSectionHeader,
      isTextOrSubcatoryNonEmpty:
          isTextOrSubcategoryNonEmpty,  // spelled wrong in the distributed v2
                                        // js library
      isTextOrSubcategoryNonEmpty,  // spelled correctly for when they fix it
      category,
      subcategory,
      title: displayTitle,
      text,
      url,
    };
  });
}

function formatCommunityHits(hits, baseUrl) {
  return hits.map(function(hit, i) {
    const url = baseUrl + hit.url
    const category = 'Sonatype Community';
    const subcategory = hit.category.name;
    const displayTitle = hit._highlightResult.topic.title.value;
    const text = hit._highlightResult.content.value;

    const isTextOrSubcategoryNonEmpty = (subcategory && subcategory !== '') ||
        (displayTitle && displayTitle !== '');
    const isLvl1EmptyOrDuplicate =
        !subcategory || subcategory === '' || subcategory === category;
    const isLvl2 =
        displayTitle && displayTitle !== '' && displayTitle !== subcategory;
    const isLvl1 = !isLvl2 &&
        (subcategory && subcategory !== '' && subcategory !== category);
    const isLvl0 = !isLvl1 && !isLvl2;
    const isHeader = (i === 0);
    return {
      isLvl0,
      isLvl1,
      isLvl2,
      isLvl1EmptyOrDuplicate,
      isCategoryHeader: isHeader,  // hit.isCategoryHeader,
      isSubCategoryHeader: hit.isSectionHeader,
      isTextOrSubcatoryNonEmpty:
          isTextOrSubcategoryNonEmpty,  // spelled wrong in the distributed v2
                                        // js library
      isTextOrSubcategoryNonEmpty,  // spelled correctly for when they fix it
      category,
      subcategory,
      title: displayTitle,
      text,
      url,
    };
  });
}

function formatZenDeskHits(hits, baseUrl) {
  // Group hits by category / subcategory
  let groupedHits = groupHits(hits);

  // Translate hits into smaller objects to be send to the template
  return groupedHits.map(function(hit, i) {
    const url = baseUrl + hit.locale.locale + '/articles/' +
        hit.id;  //`${baseUrl}${locale}/${dataset}/${suggestion.id}`;
    const category =
        'Sonatype Knowledgebase';  // hit._highlightResult.category.title.value;
    const subcategory =
        hit._highlightResult.category.title
            .value;  // hit._highlightResult.section.title.value;
    const displayTitle = hit._highlightResult.title.value;
    const text = hit._snippetResult.body_safe.value;
    const isTextOrSubcategoryNonEmpty = (subcategory && subcategory !== '') ||
        (displayTitle && displayTitle !== '');
    const isLvl1EmptyOrDuplicate =
        !subcategory || subcategory === '' || subcategory === category;
    const isLvl2 =
        displayTitle && displayTitle !== '' && displayTitle !== subcategory;
    const isLvl1 = !isLvl2 &&
        (subcategory && subcategory !== '' && subcategory !== category);
    const isLvl0 = !isLvl1 && !isLvl2;
    const isHeader = (i === 0);

    return {
      isLvl0,
      isLvl1,
      isLvl2,
      isLvl1EmptyOrDuplicate,
      isCategoryHeader: isHeader,  // hit.isCategoryHeader,
      isSubCategoryHeader: hit.isSectionHeader,
      isTextOrSubcatoryNonEmpty:
          isTextOrSubcategoryNonEmpty,  // spelled wrong in the distributed v2
                                        // js library
      isTextOrSubcategoryNonEmpty,  // spelled correctly for when they fix it
      category,
      subcategory,
      title: displayTitle,
      text,
      url,
    };
  });
}

function formatGuidesHits(hits, baseUrl) {
  return hits.map(function(hit, i) {
    const url = baseUrl + hit.url
    const category = 'Sonatype Guides';
    const subcategory =
        (hit.subsection ? hit.section + ' - ' + hit.subsection : hit.section);
    const displayTitle = hit._highlightResult.title.value;
    const text = hit._highlightResult.description.value;

    const isTextOrSubcategoryNonEmpty = (subcategory && subcategory !== '') ||
        (displayTitle && displayTitle !== '');
    const isLvl1EmptyOrDuplicate =
        !subcategory || subcategory === '' || subcategory === category;
    const isLvl2 =
        displayTitle && displayTitle !== '' && displayTitle !== subcategory;
    const isLvl1 = !isLvl2 &&
        (subcategory && subcategory !== '' && subcategory !== category);
    const isLvl0 = !isLvl1 && !isLvl2;
    const isHeader = (i === 0);
    return {
      isLvl0,
      isLvl1,
      isLvl2,
      isLvl1EmptyOrDuplicate,
      isCategoryHeader: isHeader,  // hit.isCategoryHeader,
      isSubCategoryHeader: hit.isSectionHeader,
      isTextOrSubcatoryNonEmpty:
          isTextOrSubcategoryNonEmpty,  // spelled wrong in the distributed v2
                                        // js library
      isTextOrSubcategoryNonEmpty,  // spelled correctly for when they fix it
      category,
      subcategory,
      title: displayTitle,
      text,
      url,
    };
  });
}

function groupHits(hits) {
  let groupedHits = new Map();
  hits.forEach((hit) => {
    const category = hit.category.title;
    const section = hit.section.title;

    if (!groupedHits.has(category)) {
      hit.isCategoryHeader = true;
      groupedHits.set(category, new Map());
    }
    if (!groupedHits.get(category).has(section)) {
      hit.isSectionHeader = true;
      groupedHits.get(category).set(section, []);
    }
    groupedHits.get(category).get(section).push(hit);
  });

  let flattenedHits = [];
  groupedHits.forEach((sectionsValues) => {
    sectionsValues.forEach((sectionHits) => {
      sectionHits.forEach((sectionHit) => {
        flattenedHits.push(sectionHit);
      });
    });
  });

  return flattenedHits;
}

function getQuery(indexName, hitsPerPage, searchQuery, snippet) {
  return {
    indexName: indexName, query: searchQuery, params: {
      hitsPerPage: hitsPerPage,
      highlightPreTag: '<span class="algolia-docsearch-suggestion--highlight">',
      highlightPostTag: '</span>',
      attributesToSnippet: [snippet],
      snippetEllipsisText: '...'
    }
  }
}


// full results page

var keyboardSelected = false;
var searchInput = document.getElementById('aa-search-input');

searchInput.addEventListener("keyup", function(event) {
if (event.keyCode === 13) {
  event.preventDefault();
  var term = event.target.value;
  if (!keyboardSelected && term) {
    window.open('https://my.sonatype.com/search?q=' + term, '_blank');
  }
}

});