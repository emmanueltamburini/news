-First, install Jenkins with the example of Jenkins file (it may possible that the first step will be no necesarilly)
  * You can access it by the port 8080 (by Default)
-Second, install sonarQube with the examples of Sonar folder (it exists a file with a basic configuration, it must exist a "sonar" user)
  * The sonar.service route may be /etc/systemd/system/
  * Can see service status with sudo systemctl status (you can use start and stop too)
  * You can access it by the port 9000 (by Default)
-Third, install Nexus as binary repository with the examples of Nexus folder (only if it is necesary) (it exists a file with a basic configuration, it must exist a "nexus" user)
  * You will need intall jdk-8 (openjdk works)
  * The nexus.service route may be /etc/systemd/system/
  * Can see service status with sudo systemctl status (you can use start and stop too) (this service do not check as a run)
  * You can access it by the port 8081 (by Default)
-Fourth create Jenkins jobs, for this you can use the examples in the job Jenkins folder in jenkins folder
  * You need configure SonarQube servers in http://etamburini.usr.lab.plugbox.io:8080/configure with a name and ip adress of a sonar service (example: name: sonarqube; server url: http://etamburini.usr.lab.plugbox.io:9000/)
  * You need configure JDK in http://etamburini.usr.lab.plugbox.io:8080/conficonfigureTools with a name and adress of your JDK (example: name: JDK11; JAVA_HOME: /usr/lib/jvm/java-11-openjdk-amd64)
  * You need configure Git in http://etamburini.usr.lab.plugbox.io:8080/configureTools with a name and adress of git (example: Default: sonarqube; Path to Git executable: git)
  * You need configure Maven in http://etamburini.usr.lab.plugbox.io:8080/configureTools with a name and adress of Maven (example: name: Maven; MAVEN_HOME: /usr/share/maven)
  * You need configure Docker in http://etamburini.usr.lab.plugbox.io:8080/configureTools with a name and adress of Docker (example: name: Docker; Installation root: )
  * You can set credenttials in http://etamburini.usr.lab.plugbox.io:8080/credentials/store/system/domain/_/newCredentials
  * if you want to use a bitbucket as project repository, you must install bitbucket plugin and check "Build when a change is pushed to BitBucket" in your current project
-Fifth, if you are going to use currently POM configuration to push your jar files into nexus repository you have to configure jenkins settings (example in jenkins folder (settings.xml))
-Sixth, if you are going to use currently POM configuration to push your docker files into nexus repository you have to configure jenkins daemon (example in jenkins folder (using nexus ... html))
  * Pay attetion in this configuration, it depends about your current docker version (Docker version 19.03.5)
    {
      "debug": true,
      "insecure-registries": [
        "etamburini.usr.lab.plugbox.io:8082",
        "etamburini.usr.lab.plugbox.io:8083"
      ]
    }

TIPS:

-Pay attetion of your memory, there is too many services run (free -m can be usefully for check your memory)
-http://etamburini.usr.lab.plugbox.io:8080/job/News-CI/pipeline-syntax/ can be usefully to see correct syntax in your jobs
-You can install plugins in http://etamburini.usr.lab.plugbox.io:8080/pluginManager/