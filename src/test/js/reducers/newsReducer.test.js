import reducer from '../../../main/js/reducers/newsReducer'
import * as actionTypes from '../../../main/js/constants/actionTypes'

const initialState = {
    safeUrls: [],
};

describe('Test User Reducer', () => {
    it('InitialState', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    });
	 //---------------------------------- SAVE_URL ---------------------------------------------
	describe('SAVE_URL', () => {
	    it('Save a url', () => {
            const urls = [
                'https://jestjs.io/docs/en/getting-started'
            ];

	        const response = {
	            safeUrls: urls
	        };

	        const saveUrl = {
	            type: actionTypes.SAVE_URL,
	            urls,
	        };

	        expect(reducer(initialState, saveUrl)).toEqual(response)
	    });

	    it('Save some urls', () => {
            const urls = [
                'https://jestjs.io/docs/en/getting-started',
                'https://github.com/marak/Faker.js/',
            ];

	        const response = {
	            safeUrls: urls
	        };

	        const saveUrl = {
	            type: actionTypes.SAVE_URL,
	            urls,
	        };

	        expect(reducer(initialState, saveUrl)).toEqual(response)

	    });

	    it('Save a duplicated url', () => {
            const urls = [
	           'https://jestjs.io/docs/en/getting-started',
            ];

	        const state = {
	            safeUrls: [
	                'https://jestjs.io/docs/en/getting-started',
	                'https://github.com/marak/Faker.js/',
            	],
	        };

	        const saveUrl = {
	            type: actionTypes.SAVE_URL,
	            urls,
	        };

	        expect(reducer(state, saveUrl)).toEqual(state)
	    });
	});

	 //---------------------------------- SAVE_URL ---------------------------------------------
	describe('DELETE_URL', () => {
	    it('Delete a existing url', () => {
            const url = 'https://jestjs.io/docs/en/getting-started';

	        const state = {
	            safeUrls: [
	                'https://jestjs.io/docs/en/getting-started',
	                'https://github.com/marak/Faker.js/',
            	],
	        };

	        const deleteUrl = {
	            type: actionTypes.DELETE_URL,
	            url,
	        };

	        expect(reducer(state, deleteUrl)).toEqual(state)
	    });

	    it('Delete a non-existent url', () => {
            const url = 'https://www.facebook.com/';

	        const state = {
	            safeUrls: [
	                'https://jestjs.io/docs/en/getting-started',
	                'https://github.com/marak/Faker.js/',
            	],
	        };

	        const deleteUrl = {
	            type: actionTypes.DELETE_URL,
	            url,
	        };

	        expect(reducer(state, deleteUrl)).toEqual(state)
	    });
	});
});