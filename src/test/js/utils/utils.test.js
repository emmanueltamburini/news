import * as validator  from '../../../main/js/utils/validator';

describe('Utils funcitos', () => {

    //---------------------------------- validateUrl ---------------------------------------------
    describe('validateUrl function', () => {
        it('Correct url', () => {
            const url = 'https://jestjs.io/docs/en/getting-started';
            expect(validator.validateUrl(url)).toEqual(true);
        });

        it('Incorrect url', () => {
            const url = 'asdaeqwesasasf';
            expect(validator.validateUrl(url)).toEqual(false);
        });

        it('Empty url', () => {
            const url = '';
            expect(validator.validateUrl(url)).toEqual(false);
        });
    });

    //-------------------------------- validateParameter ------------------------------------------
    describe('validateParameter function', () => {
        it('Correct paramater', () => {
            const url = 'https://jestjs.io/docs/en/getting-started';
            const word = 'palabra';
            const setAlert = (alert) => {};
            expect(validator.validateParameter(url, word, setAlert)).toEqual(true);
        });

        it('Incorrect url paramater', () => {
            const url = 'asdqwqe';
            const word = 'palabra';
            const setAlert = (alert) => {};
            expect(validator.validateParameter(url, word, setAlert)).toEqual(false);
        });

        it('Incorrect word', () => {
            const url = 'https://jestjs.io/docs/en/getting-started';
            const word = '';
            const setAlert = (alert) => {};
            expect(validator.validateParameter(url, word, setAlert)).toEqual(false);
        });

        it('Incorrect word 2', () => {
            const url = 'https://jestjs.io/docs/en/getting-started';
            const word = 'asdfghjklqwertyw';
            const setAlert = (alert) => {};
            expect(validator.validateParameter(url, word, setAlert)).toEqual(false);
        });
    });
});