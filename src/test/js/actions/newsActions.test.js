import * as newsActions  from '../../../main/js/actions/newsActions';
import * as actionTypes from '../../../main/js/constants/actionTypes';
import * as urls from '../../../main/js/constants/urls';
import moxios from 'moxios';

describe('User actions', () => {

    beforeEach(() => {
        moxios.install()
    });

    afterEach(() => {
        moxios.uninstall()
    });

    // --------------------------------------Actions-----------------------------------------------

    describe('Actions', () => {
        it('pushSafeURLS returns a correct JSON', () => {
            const urls = 'https://jestjs.io/docs/en/getting-started';
            const expectedAction = {
                type: actionTypes.SAVE_URL,
                urls,
            };
            expect(newsActions.pushSafeURLS(urls)).toEqual(expectedAction);
        });

        it('pullSafeURLS returns a correct JSON', () => {
            const url = 'https://jestjs.io/docs/en/getting-started';
            const expectedAction = {
                type: actionTypes.DELETE_URL,
                url,
            }
            expect(newsActions.pullSafeURLS(url)).toEqual(expectedAction);
        });
    });
    // -----------------------------------------Post ------------------------------------------

    describe('POST', () => {
        it('Accepted Post News', () => {
            const body = { url : 'https://jestjs.io/docs/en/getting-started', word: "test" };

            const store = mockStore({});
            moxios.stubRequest("/api/content/check", {
                status: 200,
                response: {
                    state: 'accepted'
                }
            });

            const expectedActions = [
                {
                    type: actionTypes.SAVE_URL,
                    urls: [body.url],
                }
            ];

            return newsActions.postNews(store.dispatch, body)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Reject Post News', () => {
            const body = { url : 'https://jestjs.io/docs/en/getting-started', word: "test" };

            const store = mockStore({});
            moxios.stubRequest("/api/content/check", {
                status: 200,
                response: {
                    state: 'reject'
                }
            });

            const expectedActions = [];

            return newsActions.postNews(store.dispatch, body)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Wrong Status Post News', () => {
            const body = { url : 'https://jestjs.io/docs/en/getting-started', word: "test" };

            const store = mockStore({});
            moxios.stubRequest("/api/content/check", {
                status: 500,
                response: {
                    state: 'reject'
                }
            });

            const expectedActions = [];

            return newsActions.postNews(store.dispatch, body)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Wrong Response Post News', () => {
            const body = { url : 'https://jestjs.io/docs/en/getting-started', word: "test" };

            const store = mockStore({});
            moxios.stubRequest("/api/content/check", null);

            const expectedActions = [];

            return newsActions.postNews(store.dispatch, body)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });
    });
    // -----------------------------------------GET ------------------------------------------
    describe('GET', () => {
        it('Accepted GET News', () => {
            const response = [
                { url : 'https://jestjs.io/docs/en/getting-started', word: "test" },
                { url : 'https://jestjs.io/docs/en/getting-started', word: "test" }
            ];

            const store = mockStore({});
            moxios.stubRequest("/api/content", {
                status: 200,
                response: response
            });

            const expectedActions = [
                {
                    type: actionTypes.SAVE_URL,
                    urls: response,
                }
            ];

            return newsActions.getNews(store.dispatch)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Accepted empty GET News', () => {
            const store = mockStore({});
            moxios.stubRequest("/api/content", {
                status: 204
            });

            const expectedActions = [];

            return newsActions.getNews(store.dispatch)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Wrong Status Get News', () => {
            const store = mockStore({});
            moxios.stubRequest("/api/content", {
                status: 500
            });

            const expectedActions = [];

            return newsActions.getNews(store.dispatch)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Wrong Response Get News', () => {
            const store = mockStore({});
            moxios.stubRequest("/api/content", null);

            const expectedActions = [];

            return newsActions.getNews(store.dispatch)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });
    });

    // -----------------------------------------DELETE ------------------------------------------
    describe('DELETE', () => {
        it('Accepted Delete News', () => {
            const body = 'https://jestjs.io/docs/en/getting-started';

            const store = mockStore({});
            moxios.stubRequest("/api/content", {
                status: 200,
            });

            const expectedActions = [
                {
                    type: actionTypes.DELETE_URL,
                    url: body,
                }
            ];

            return newsActions.deleteNews(store.dispatch, body)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Denied Delete News', () => {
            const body = 'https://jestjs.io/docs/en/getting-started';

            const store = mockStore({});
            moxios.stubRequest("/api/content", {
                status: 204,
            });

            const expectedActions = [];

            return newsActions.deleteNews(store.dispatch, body)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Wrong Status Delete News', () => {
            const store = mockStore({});
            moxios.stubRequest("/api/content", {
                status: 500
            });

            const expectedActions = [];

            return newsActions.deleteNews(store.dispatch)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });

        it('Wrong Response Delete News', () => {
            const store = mockStore({});
            moxios.stubRequest("/api/content", null);

            const expectedActions = [];

            return newsActions.getNews(store.dispatch)
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions)
            })
        });
    });

    // -----------------------------------------GET instance------------------------------------------
    describe('GET instance', () => {
        it('200 GET instance', () => {
            const response = 'instance';
            moxios.stubRequest('/api/instance', {
                status: 200,
                response: response
            });

            return newsActions.instance()
                .then((myResponse) => {
                    expect(myResponse).toEqual(response)
            })
        });

        it('No 200 GET instance', () => {
            moxios.stubRequest('/api/instance', {
                status: 201,
            });

            return newsActions.instance()
                .then((myResponse) => {
                    expect(myResponse).toEqual(null)
            })
        });

        it('Wrong GET instance', () => {
            moxios.stubRequest('/api/instance', null);

            return newsActions.instance()
                .then((myResponse) => {
                    expect(myResponse).toEqual(null)
            })
        });
    });

    // -----------------------------------------GET version------------------------------------------
    describe('GET version', () => {
        it('200 GET version', () => {
            const response = 'version';
            moxios.stubRequest('/api/version', {
                status: 200,
                response: response
            });

            return newsActions.version()
                .then((myResponse) => {
                    expect(myResponse).toEqual(response)
            })
        });

        it('No 200 GET version', () => {
            moxios.stubRequest('/api/version', {
                status: 201,
            });

            return newsActions.version()
                .then((myResponse) => {
                    expect(myResponse).toEqual(null)
            })
        });

        it('Wrong GET version', () => {
            moxios.stubRequest('/api/version', null);

            return newsActions.version()
                .then((myResponse) => {
                    expect(myResponse).toEqual(null)
            })
        });
    });
});