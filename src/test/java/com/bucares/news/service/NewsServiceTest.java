package com.bucares.news.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.github.javafaker.Faker;
import com.bucares.news.entity.News;
import com.bucares.news.repository.NewsRepository;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
public class NewsServiceTest extends TestCase {
	final static private Faker faker = new Faker();
	@TestConfiguration
    static class NewsServiceContextConfiguration {

        @Bean
        public NewsService newService() {
            return new NewsService();
        }
    }

	@Autowired
	private NewsService newsService;

	@MockBean
	private NewsRepository newsRepository;

	@MockBean
	private RestTemplate restTemplate;

	@Test
	public void checkCorrectWordIntoNoBDUrlTest() throws Exception {
		String url = faker.internet().url();
		String word = faker.lorem().characters(1, 15);
		String text = faker.lorem().paragraph() + word;
		News body = new News (url, word);
		when(restTemplate.getForEntity(any(String.class), any(Class.class)))
	          .thenReturn(new ResponseEntity<String>(text, HttpStatus.OK));
		when(newsRepository.findByUrl(any(String.class))).thenReturn(null);
		when(newsRepository.save(any(News.class))).thenReturn(null);

		assertEquals(newsService.post(body), true);
	}

	@Test
	public void checkCorrectWordIntoBDUrlTest() throws Exception {
		String url = faker.internet().url();
		String word = faker.lorem().characters(1, 15);
		String text = faker.lorem().paragraph() + word;
		News body = new News (url, word);
		when(restTemplate.getForEntity(any(String.class), any(Class.class)))
        .thenReturn(new ResponseEntity<String>(text, HttpStatus.OK));
		when(newsRepository.findByUrl(any(String.class))).thenReturn(new News(url, word));
		when(newsRepository.save(any(News.class))).thenReturn(null);

		assertEquals(newsService.post(body), true);
	}

	@Test
	public void checkIncorrectWordTest() throws Exception {
		String word = faker.lorem().characters(1, 15);
		String text = faker.lorem().paragraph();
		String url = faker.internet().url();
		News body = new News (url, word);
		when(restTemplate.getForEntity(any(String.class), any(Class.class)))
        .thenReturn(new ResponseEntity<String>(text, HttpStatus.OK));
		when(newsRepository.findByUrl(any(String.class))).thenReturn(null);
		when(newsRepository.save(any(News.class))).thenReturn(null);

		assertEquals(newsService.post(body), false);
	}

	@Test
	public void checkExceptionTest() throws Exception {
		String word = faker.lorem().characters(1, 15);
		String url = faker.internet().url();
		News body = new News (url, word);
		when(restTemplate.getForEntity(any(String.class), any(Class.class)))
        .thenReturn(null);
		when(newsRepository.findByUrl(any(String.class))).thenReturn(null);
		when(newsRepository.save(any(News.class))).thenReturn(null);

		assertEquals(newsService.post(body), true);
	}

	@Test
	public void getTest() throws Exception {
		when(newsRepository.findByAll()).thenReturn(new ArrayList<String> ());
		assertEquals(newsService.get() instanceof List<?>, true);
	}

	@Test
	public void deleteBDUrlTest() throws Exception {
		String url = faker.internet().url();
		News body = new News (url);
		when(newsRepository.findByUrl(any(String.class))).thenReturn(new News(url));
		assertEquals(newsService.delete(body), true);
	}

	@Test
	public void deleteNoBDUrlTest() throws Exception {
		String url = faker.internet().url();
		News body = new News (url);
		when(newsRepository.findByUrl(any(String.class))).thenReturn(null);
		assertEquals(newsService.delete(body),false);
	}
}
