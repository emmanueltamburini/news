package com.bucares.news.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.bucares.news.entity.News;
import com.bucares.news.service.NewsService;

@RunWith(SpringRunner.class)
@WebMvcTest(NewsController.class)
public class NewsControllerTest {
	final static private Faker faker = new Faker();

	@MockBean
	private NewsService newsService;
	
    @Autowired
    private MockMvc mvc;
    
    private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void checkCorrectWordIntoUrlPostControllerTest() throws Exception {
		when(newsService.post(any(News.class))).thenReturn(true);
		mvc.perform(post("/api/content/check")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(new News(faker.internet().url(), faker.lorem().characters(1, 15)))))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.state").value("rejected"));
	}

	@Test
	public void checkIncorrectWordIntoUrlPostControllerTest() throws Exception {
		when(newsService.post(any(News.class))).thenReturn(false);
		mvc.perform(post("/api/content/check")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(new News(faker.internet().url(), faker.lorem().characters(1, 15)))))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.state").value("accepted"));
	}

	@Test
	public void getEmptyListControllerTest() throws Exception {
		List<String> news = new ArrayList<String>();
		when(newsService.get()).thenReturn(news);
		mvc.perform(get("/api/content")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(news)))
        	.andExpect(status().isNoContent());
	}

	@Test
	public void getNoEmptyListControllerTest() throws Exception {
		List<String> news = new ArrayList<String>();
		news.add(faker.internet().url());
		when(newsService.get()).thenReturn(news);
		mvc.perform(get("/api/content")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(news)))
        	.andExpect(status().isOk());
	}

	@Test
	public void deleteBDURLControllerTest() throws Exception {
		when(newsService.delete(any(News.class))).thenReturn(true);
		mvc.perform(delete("/api/content")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(new News(faker.internet().url()))))
        	.andExpect(status().isOk());
	}
	
	@Test
	public void deleteNoBDURLControllerTest() throws Exception {
		when(newsService.delete(any(News.class))).thenReturn(false);
		mvc.perform(delete("/api/content")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(new News(faker.internet().url()))))
        	.andExpect(status().isNoContent());
	}

	@Test
	public void getInstanceTest() throws Exception {
		mvc.perform(get("/api/instance")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE))
        	.andExpect(status().isOk());
	}

	@Test
	public void getVersionTest() throws Exception {
		mvc.perform(get("/api/version")
        		.accept(MediaType.APPLICATION_JSON_VALUE)
        		.contentType(MediaType.APPLICATION_JSON_VALUE))
        	.andExpect(status().isOk());
	}
}
