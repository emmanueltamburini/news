package com.bucares.news;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class NewsApplicationTest {

	@Test
	public void contextLoads() {
		NewsApplication aux = new NewsApplication ();
		assertTrue(aux instanceof NewsApplication);
	}

}
