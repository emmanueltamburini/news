package com.bucares.news.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.javafaker.Faker;

@RunWith(SpringRunner.class)
public class NewsTest {
	final static private Faker faker = new Faker();
	@Test
	public void defaultConstructorTest() {
		News aux = new News ();
		assertNotNull(aux);
		assertTrue(aux instanceof News);
	}

	@Test
	public void oneParameterConstructorTest() {
		String url = faker.internet().url();
		News aux = new News (url);
		assertNotNull(aux);
		assertTrue(aux instanceof News);
	}
	
	@Test
	public void twoParameterConstructorTest() {
		String url = faker.internet().url();
		String word = faker.lorem().characters(1, 15);
		News aux = new News (url, word);
		assertNotNull(aux);
		assertTrue(aux instanceof News);
	}

	@Test
	public void getAndSetIdTest() {
		long id = faker.number().numberBetween(100, 200);
		News aux = new News ();
		aux.setId(id);
		assertEquals(id, aux.getId());
	}

	@Test
	public void getAndSetUrl() {
		String url = faker.internet().url();
		News aux = new News ();
		aux.setUrl(url);
		assertEquals(url, aux.getUrl());
	}

	@Test
	public void getAndSetWord() {
		String word = faker.lorem().characters(1, 15);
		News aux = new News ();
		aux.setWord(word);
		assertEquals(word, aux.getWord());
	}
}
