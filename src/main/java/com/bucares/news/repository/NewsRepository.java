package com.bucares.news.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bucares.news.entity.News;

interface ProjectIdAndName{
    String getId();
    String getName();
}

@Repository
public interface NewsRepository extends JpaRepository<News, Serializable>{
	News findByUrl(String url);
	
	@Query("select url from News")
	List<String> findByAll();

}
