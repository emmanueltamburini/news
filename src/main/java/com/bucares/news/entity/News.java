package com.bucares.news.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.bucares.news.model.NewsModel;

@Table(name="News")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class News implements Serializable{
	public News(String url) {
		this(url, null);
	}
	
	public News(String url, String word) {
		super();
		this.url = url;
		this.word = word;
	}

	public News(NewsModel newsModel) {
        this.id = newsModel.getId();
        this.url = newsModel.getUrl();
        this.word = newsModel.getWord();
	}

	private static final long serialVersionUID = 1L;

	@GeneratedValue
	@Id
	@Column(name="id")
	private long id;

	@Column(name="url")
	private String url;

	@Column(name="word")
	private String word;
}
