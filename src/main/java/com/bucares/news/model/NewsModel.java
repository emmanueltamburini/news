package com.bucares.news.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NewsModel {
	private long id;

	private String url;

	private String word;
}
