package com.bucares.news;

public class URLConstants {
    public static final String MAIN = "/api";
    public static final String POST = "/content/check";
    public static final String GET = "/content";
    public static final String DELETE = "/content";
    public static final String INSTANCE = "/instance";
    public static final String VERSION = "/version";


    private URLConstants() { /* empty */}
}