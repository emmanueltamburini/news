package com.bucares.news.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;

import com.bucares.news.entity.News;
import com.bucares.news.model.NewsModel;
import com.bucares.news.service.NewsService;

import static com.bucares.news.URLConstants.MAIN;
import static com.bucares.news.URLConstants.POST;
import static com.bucares.news.URLConstants.GET;
import static com.bucares.news.URLConstants.DELETE;
import static com.bucares.news.URLConstants.INSTANCE;
import static com.bucares.news.URLConstants.VERSION;	

@RestController
@RequestMapping(MAIN)
public class NewsController {

	@Autowired
	private NewsService service;

	@Value("${instance}")
	private String instance;

	@Value("${build.version}")
	private String version;

	@PostMapping(POST)
	public ResponseEntity<Map<String, String>> checkUrl(@RequestBody @Valid NewsModel body) {
		News constBody = new News(body);
		Map<String, String> response = new HashMap<>();
        response.put("state", service.post(constBody) ? "rejected" : "accepted");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping(GET)
	public ResponseEntity<List<String>> getNews() {
		List<String> news = service.get();
		return new ResponseEntity<>(news, news.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK);
	}

	@DeleteMapping(DELETE)
	public ResponseEntity<Object> deleteNew(@RequestBody @Valid NewsModel body) {
		News constBody = new News(body);
		return new ResponseEntity<>(true, service.delete(constBody) ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}

	@GetMapping(INSTANCE)
	public ResponseEntity<Object> instance() {
		return new ResponseEntity<>(instance,HttpStatus.OK);
	}

	@GetMapping(VERSION)
	public ResponseEntity<Object> version() {
		return new ResponseEntity<>(version,HttpStatus.OK);
	}
}
