package com.bucares.news.service;
import java.util.List;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bucares.news.entity.News;
import com.bucares.news.repository.NewsRepository;

@Service
public class NewsService {

	@Autowired
	private NewsRepository repository;

	private static final Log logger = LogFactory.getLog(NewsService.class);

	@Autowired
	private RestTemplate restTemplate;

	public boolean post(News body) {
		ResponseEntity<String> response;
		try {
			response = restTemplate.getForEntity(body.getUrl(), String.class);
			if (response.getBody().contains(body.getWord())) {
				logger.info("Word is in url");
				logger.error("News has not saved in data base");
				return true;
			}

			logger.info("Word is not in url");
			if (repository.findByUrl(body.getUrl()) == null) {
				logger.info("News has saved in data base");
				repository.save(body);
			}
			return false;
		}catch(Exception exception){
			logger.error("Exception");
			return  true;
		}
	}

	public List<String> get() {
		logger.info("News has been sent");
		return repository.findByAll();
	}

	public boolean delete(News body) {
		News news = repository.findByUrl(body.getUrl());
		if (news != null) {
			repository.delete(news);
			logger.info("News has been deleted");
			return true;
		}
		logger.error("There is not news with this url: " + body.getUrl());
		return false;
	}
}
