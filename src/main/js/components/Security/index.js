import React, { Component } from 'react';
import Table from 'react-bootstrap/Table'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button'

import { deleteNews } from '../../actions/newsActions';

import 'bootstrap/dist/css/bootstrap.min.css';

class Security extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(url) {
    const {
      dispatch,
      setAlert,
    } =this.props;

    deleteNews(dispatch, url)
    .then((response) =>
      setAlert(`${url} has been deleted in security urls`, 'danger'))
    .catch( () => {
      setAlert('Something is wrong', 'danger');
    });
  }

  render() {
     const {
      safeUrls,
    } = this.props;

    return (
      <div>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Security Url</th>
            </tr>
          </thead>
          <tbody>
          {
            safeUrls.map((element) => (
              <tr
                key={`${element}`}
              >
                <td>
                {element}
                  <Button variant="danger" onClick={() => this.onClick(element)}>
                    Delete
                  </Button>
                </td>
              </tr>
            ))
          }
          </tbody>
        </Table>
      </div>
    )
  }
}

Security.defaultProps = {
  safeUrls: [],
};

Security.propTypes = {
  safeUrls: PropTypes.arrayOf(PropTypes.string),
  setAlert: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    safeUrls: state.news.safeUrls,
  };
}

export default connect(mapStateToProps)(Security);