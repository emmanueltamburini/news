import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Alert from 'react-bootstrap/Alert'

import Form from '../Form';
import Security from '../Security';
import {
  instance,
  version,
} from '../../actions/newsActions';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/Main.css';

const variants = [
  'primary',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'light',
  'dark',
];

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      variant: 'success',
      message: '',
      instance: null,
      version: null,
    };
    this.setShow = this.setShow.bind(this);
    this.setAlert = this.setAlert.bind(this);
  }

  componentDidMount() {
    instance()
    .then((instance) => this.setState({ instance }));
  
    version()
    .then((version) => this.setState({ version }));
  }

  setShow (show) {
    this.setState({ show })
  }

  setAlert (message, variant = 'success') {
    if (variants.indexOf(variant) === -1) {
      return false;
    }

    this.setState({
      show: true,
      message,
      variant,
    })

    setTimeout(() => {
      this.setState({ show: false });
    }, 10000);

    return true;
  }

  render() {
    const {
      show,
      variant,
      message,
      instance,
      version,
    } = this.state;

    return (
      <Container fluid className="Main">
        <Row className="justify-content-md-center">
          <Col md={{ offset: 3 }}>
            <h3>{`News Checker: instance (${instance}), version (${version})`}</h3>
          </Col>
        </Row>
        <Row>
          <Alert show={show}  variant={variant} onClose={() => this.setShow(false)} dismissible>
            {message}
          </Alert>
        </Row>
        <Row>
          <Col>
            <Tabs id="controlled-tab-example">
              <Tab eventKey="home" title="Url check">
                <Form setAlert={this.setAlert} />
              </Tab>
              <Tab eventKey="profile" title="Url security">
                <Security setAlert={this.setAlert}/>
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default connect()(Main);