import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button'
import Spinner from 'react-bootstrap/Spinner'

import { validateParameter } from '../../utils/validator';
import {
  postNews,
  getNews,
} from '../../actions/newsActions';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      word: '',
      spinner: false,
    };
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  componentDidMount () {
    const { dispatch } = this.props;
    getNews(dispatch);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  async onClick(e) {
    const {
      url,
      word,
    } = this.state;

    const {
      dispatch,
      setAlert,
    } =this.props;

    if (!validateParameter(url, word, setAlert)) {
      return;
    }

    const body = {
      url,
      word,
    }

    await this.setState({ spinner: true });

    await postNews(dispatch, body)
    .then((response) =>
      response ?
          setAlert(`Accepted: ${word} is not in ${url} , url has been added in security urls`)
        :
          setAlert(`Rejected: ${word} is in ${url}`, 'info')
      )
    .catch( () => {
      setAlert('Something is wrong', 'danger');
    });

    await this.setState({ spinner: false });

    this.setState({
      url: '',
      word: '',
    })
  }

  render() {
    const {
      url,
      word,
      spinner,
    } = this.state;

    return (
      <div>
        <div className="row m-2">
          <div className="col-12 col-md-6 p-2">
            <label htmlFor="url">
              {'Url '}
              <input
                name="url"
                type="text"
                value={url}
                onChange={this.onChange}
              />
            </label>
          </div>
          <div className="col-12 col-md-6 p-2">
            <label htmlFor="word">
              {'Word '}
              <input
                name="word"
                type="text"
                value={word}
                onChange={this.onChange}
              />
            </label>
          </div>
          <div className="w-50">
			      <Button variant="primary" onClick={this.onClick}>
              {spinner&&
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
              }
			        {spinner ? 'Loanding' : 'Send'}
			      </Button>
          </div>
        </div>
      </div>
    )
  }
}

Form.propTypes = {
  dispatch: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
};

export default connect()(Form);