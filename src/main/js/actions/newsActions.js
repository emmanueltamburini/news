import {
	SAVE_URL,
	DELETE_URL,
	} from '../constants/actionTypes';

import {
	POST_NEWS,
	GET_NEWS,
    DELETE_NEWS,
    INSTANCE,
    VERSION,
} from '../constants/urls';
import axios from "axios";

export function pushSafeURLS(urls) {
  return {
    type: SAVE_URL,
    urls,
  };
}

export function pullSafeURLS(url) {
  return {
    type: DELETE_URL,
    url,
  };
}

export function postNews(dispatch, body) {
    return  axios.post(POST_NEWS, body)
        .then( response => {
        	if (response.status === 200 && response.data.state === 'accepted') {
				dispatch(pushSafeURLS([body.url]));
                return true;
        	}
            return false;
        })
        .catch( (err) => {
            return false;
        })
}

export function getNews(dispatch) {
    return  axios.get(GET_NEWS)
        .then( response => {
        	if (response.status === 200) {
        		dispatch(pushSafeURLS([...response.data]));
				return true;
        	}
            return false;
        })
        .catch( (err) => {
            return err;
        })
}

export function deleteNews(dispatch, url) {
	const body = {
		data: {
			url,
			word: null,
		}
	};
    return  axios.delete(DELETE_NEWS, body)
        .then( response => {
        	if (response.status === 200) {
        		dispatch(pullSafeURLS(url));
				return true;
        	}
            return false;
        })
        .catch( (err) => {
            return false;
        })
}

export function instance() {
    return axios.get(INSTANCE)
        .then( response => {
        	if (response.status === 200) {
                return response.data;
        	}
            return null;
        })
        .catch( (err) => {
            return null;
        })
}

export function version() {
    return axios.get(VERSION)
        .then( response => {
        	if (response.status === 200) {
                return response.data;
        	}
            return null;
        })
        .catch( (err) => {
            return null;
        })
}