export const POST_NEWS = '/api/content/check';
export const GET_NEWS = '/api/content';
export const DELETE_NEWS = '/api/content';
export const INSTANCE = '/api/instance';
export const VERSION = '/api/version';


