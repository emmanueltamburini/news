import * as actionTypes from '../constants/actionTypes'

const initialState = {
    safeUrls: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SAVE_URL:
    	const urls = [...state.safeUrls, ...action.urls]
		return {
			safeUrls: [...new Set(urls)],
		};

    case actionTypes.DELETE_URL:
		const index = state.safeUrls.indexOf(action.url);
		if (index !== -1) {
			state.safeUrls.splice(index,1);
		}

		return {
			safeUrls: [...state.safeUrls],
		};

    default: return state;
  }
};
