import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from '../reducers';

const middlewares = [
  reduxThunk,
];

export default () => createStore(reducers, composeWithDevTools(applyMiddleware(...middlewares)));
