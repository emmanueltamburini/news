export function validateUrl(url) {
	if (!url) {
		return false
	}
	const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi;
	const regex = new RegExp(expression);
	return regex.test(url)
}

export function validateParameter (url , word, setAlert) {
	if (!validateUrl(url)) {
		setAlert('Url is not validated', 'danger');
		return false;
	}
	if (word.length === 0 || word.length > 15){
		setAlert('Word must have at least 1 letter and maximum 15 letters', 'danger');
		return false;
	}
	return true;
}
