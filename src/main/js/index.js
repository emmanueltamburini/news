import React, {Suspense} from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { I18nextProvider } from 'react-i18next';
import i18n from "./i18n";

import './index.css';
import App from './App';
import getStore from './store/store';

const store = getStore();

render(
    <Provider store={store}>
        <I18nextProvider i18n={i18n}>
            <Suspense fallback="">
                <App/>
            </Suspense>
        </I18nextProvider>
    </Provider>,
    document.getElementById('react')
);
